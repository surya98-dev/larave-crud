<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PostController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index()
    {
    	// mengambil data dari table pegawai
    	$pegawai = DB::table('posts')->get();

    	// mengirim data pegawai ke view index
    	return view('index',['pegawai' => $pegawai]);

    }

    public function tambah()
    {
        //memanggil view tambah
        return view ('tambah');
    }

    //method for insert data
    public function store(Request $request)
    {
        //insert data ke db pegawai
        DB::table('posts')->insert([
            'nik' => $request->nik,
            'name' => $request->nama,
            'jabatan' => $request->jabatan,
            'umur' => $request->umur,
            'alamat' => $request->alamat
        ]);
        //redirect ke /pegawai
        return redirect('/pegawai');
    }

    //methon for edit data pegawai
    public function edit($id)
    {
        //mengambil data pegawai berdasarkan id
        $pegawai = DB::table ('posts')->where('id', $id)->get();
        //passing data pegawai yang didapat ke view edit.blade.php
        return view('edit',['pegawai' => $pegawai]);   
    }

    //update data pegawai
    // public function update(Request $request)
    // {
    //     //update data pegawai
    //     DB::table('posts')->where('id')->update ([
    //         'nik' => $request->nik,
    //         'name' => $request->nama,
    //         'jabatan' => $request->jabatan,
    //         'umur' => $request->umur,
    //         'alamat' => $request->alamat
    //     ]);
    //     //alihkan halaman ke halaman pegawai
    //     return redirect ('/pegawai');
    // }
    public function update(Request $request, $id)
    {
        $model = DB::find($id);
        $model->nik = $request->nik;
        $model->name = $request->name;
        $model->jabatan = $request->jabatan;
        $model->umur = $request->umur;
        $model->alamat = $request->alamat;
        $model->save();
        return redirect()->back()->with('success','Data updated successfully');
    }

    //method untuk menghapus data pegawai
    public function hapus($id)
    {
        //menghapus data pegawai berdasarkan id yang dipilih
        DB::table('posts')->where('id',$id)->delete();

        //alihkan halaman ke halaman pegawai
        return redirect ('/pegawai');
    }
}