<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use RealRashid\SweetAlert\Facades\Alert;

class LoginController extends Controller
{
    //login view
    public function index()
    {
        return view('auth.login');
    }

    //login post
    public function authenticate(Request $request)
    {   
        // if(Auth::attempt($request->only('email', 'password'))){
        //     return redirect('/home');
        // }
        $attibutes = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        // $credential = ['email' => $request->email, 'password' => $request->password];
        // $credential = $request->only('email', 'password');
        // dd($credential);
        if (auth::attempt($attibutes)){
            return redirect('/home/posts')->with('success','Login Success!');

        }
        throw ValidationException::withMessages([
            'email' => 'Email Not Exxist! or Worng Password',
        ]);
    }

    //logout post
    public function logout()
    {
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        Alert::warning('Logout Account');
        return redirect('/signin');
    }
}
