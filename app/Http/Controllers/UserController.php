<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    public function index()
    {
        return view('auth.register', [
            'title' => 'Register',
            'active' => 'register'
        ]);
    }

    public function profile()
    {
        return view('profile');
    }
    public function signin()
    {
        return view('auth.login', [

        ]);
    }

    // public function store(Request $request) 
    // {
    //     //insert data registrasi user
    //     DB::table('users')->insert([
    //         'email' => $request->email,
    //         'name' => $request->name,
    //         'first_name' => $request->first_name,
    //         'last_name' => $request->last_name,
    //         'handphone' => $request->handphone,
    //         'Password' => $request->password,
    //         'Confirm_Password' => $request->confrim_password
    //     ]);

    //     return redirect('/signin')->with('succes', 'Registrasi Berhasil');
    // }

    // public function authenticate(Request $request)
    // {
    //     //login user
    //     BD::table('users')
    // }

    //registrasi
    // public function store(Request $request) 
    // {
    //     //insert data registrasi user
    //     $validatedData = $request->validate([
    //         'email' => 'required|email:dns|unique:users',
    //         'username' => 'required|max:255',
    //         'first_name' => 'required|max:255',
    //         'last_name' => 'required|max:255',
    //         'handphone' => 'required|min:12',
    //         'password' => 'required|min:5|max:255',
    //         'confirm_password' => 'required|min:5|max:255'
    //     ]);

    //     //sembunyikan password dalam database
    //     // $validatedData['Password'] = bcrypt($validatedData ['Password']);
    //     $validatedData['password'] = Hash::make($validatedData['password']);
    //     // $validatedData['Confirm_Password'] = Hash::make($validatedData['Confirm_Password']);

    //     //koneksi ke database dan redirect url with alert succes
    //     User::create($validatedData);
    //     // $request->session()->flash('succes' ,'Registrasi Berhasil');
    //     return redirect('/signin')->with('succes' ,'Registrasi Berhasil');
    // }

    // login 
    // public function authenticate(Request $request)
    // {   
    //     //function post data user
    //     $credentials = $request->validate([
    //         'email' => 'required|email:dns',
    //         'password' => 'required'
    //     ]);

    //     //otentifikasi dari laravel
    //     if (Auth::attempt($credentials)) {
    //         $request->session()->regenerate();
    //         return redirect()->intended('/admin');
    //     }

    //     //redirect url dengan alert succes
    //     return back()->with('loginError', 'login Gagal');
    // }
}
