<?php

namespace App\Http\Controllers;

use App\Models\Post;
// use App\Models\Product;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class DashboardPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //menampilkan dashboard data crud
        return view('dashboard.posts.index',[
            'posts' => Post::all()
            // 'posts' => Post::where('id' , auth()->user()->id)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $user_id = auth()->user()->id;
        // $posts = Post::where('id', $user_id)->get();

        //menampilkan form create
        return view('dashboard.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi data
        $this->validate($request, [
            'nik' => 'required|min:16|max:16|unique:posts',
            'name' => 'required|max:225',
            'jeniskelamin' => 'required',
            'nohp' => 'required|min:12|max:13',
            'umur' => 'required|min:2|max:2',
            'jabatan' => 'required', 
            'alamat' => 'required|max:255',
            'image' => 'required|mimes:jpg,jpeg,png',
        ]);

        //insert image ke local public
        $file_name = $request->image->getClientOriginalName();
        $image = $request->image->storeAs('thumbnali', $file_name);

        //insert data ke db pegawai
        $post = Post::create([
            'nik' => $request->nik,
            'name' => $request->name,
            'jeniskelamin' => $request->jeniskelamin,
            'nohp' => $request->nohp,
            'umur' => $request->umur,
            'jabatan' => $request->jabatan,
            'alamat' => $request->alamat,
            'image' => $image,
            // 'image' => $request->image,
        ]);
        if ($post) {
            return redirect('/home/posts')->with('success', 'Data Created Successfully!');
        } else {
            return redirect()->back()->withinput()->with('error', 'Some Problem');
        }

                // return $request->file('image')->store('img-pegawai');
                // Post::create($data);
                // $data::create($request->all());
                //redirect ke /pegawai
                // return redirect('/home/posts')->with('success', 'Data Has Been Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //menampilkan spesifik data pegawai
        return view('dashboard.posts.show', [
            'posts' => $post
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan edit data pegawai
        $post = Post::findOrFail($id);
            return view('dashboard.posts.edit', compact('post'));


        // return view('dashboard.posts.edit',[
        //     'post' => $post
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        // dd($request->all()); 
        // $post = Post::where($post->id)->first();
        // $file_name = $request->image->getClientOriginalName();
        // $image = $request->image->storeAs('thumbnali', $file_name);
        // $post->update([
        //     'nik' => $request->nik,
        //     'name' => $request->name,
        //     'jeniskelamin' => $request->jeniskelamin,
        //     'nohp' => $request->nohp,
        //     'umur' => $request->umur,
        //     'jabatan' => $request->jabatan,
        //     'alamat' => $request->alamat,
        //     'image' => $image,
        // ]);

        // validasi data pegawai
        $request->validate([
            // 'nik' => 'required|min:16|max:16|unique:posts',
            'name' => 'required|max:225',
            'jeniskelamin' => 'required',
            'nohp' => 'required|min:11|max:13',
            'umur' => 'required|min:2|max:2',
            'jabatan' => 'required', 
            'alamat' => 'required|max:255',
            'image' => 'required|mimes:jpg,jpeg,png'
        ]);

        //save image pada local pubilc
        $file_name = $request->image->getClientOriginalName();
        $image = $request->image->storeAs('thumbnali', $file_name);

        // $input = $request->all();

        // if ($image = $request->file('image')) {
        //     $imageDestinationPath = 'uploads/';
        //     $postImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
        //     $image->move($imageDestinationPath, $postImage);
        //     $input['image'] = "$postImage";
        // }else{
        //     unset ($input['image']);
        // }
        // $post->update($input);

        //update form ke database
        $post = Post::findOrFail($id);
        $post->update([
            // 'nik' => $request->nik,
            'name' => $request->name,
            'jeniskelamin' => $request->jeniskelamin,
            'nohp' => $request->nohp,
            'umur' => $request->umur,
            'jabatan' => $request->jabatan,
            'alamat' => $request->alamat,
            'image' => $image,
        ]);
        // return redirect dengan messages
        if ($post) {
            return redirect('/home/posts')->with('success', 'Data Has Been Updated');
        } else {
            return redirect()->back()->with('error', 'Some Problem, Try Again');
        }


        // $request->validate([
        //     'nik' => 'required|min:16|max:16|unique:posts',
        //         'name' => 'required|max:225',
        //         'jeniskelamin' => 'required',
        //         'nohp' => 'required|min:12|max:13',
        //         'umur' => 'required|min:2|max:2',
        //         'jabatan' => 'required', 
        //         'alamat' => 'required|max:255',
        //         'image' => 'required|mimes:jpg,jpeg,png'
        // ]);
  
        // $input = $request->all();
  
        // if ($image = $request->file('image')) {
        //     $destinationPath = 'image/';
        //     $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
        //     $image->move($destinationPath, $profileImage);
        //     $input['image'] = "$profileImage";
        // }else{
        //     unset($input['image']);
        // }
          
        // $product->update($input);
    
        // return redirect()->route('home.posts') ->with('success','Data updated successfully');

        // $data = $request->validate([
        //     'nik' => 'required||min:16|max:16|unique:posts',
        //     'name' => 'required|max:225',
        //     'jeniskelamin' => 'required',
        //     'nohp' => 'required|min:12|max:13',
        //     'umur' => 'required|min:2|max:2',
        //     'jabatan' => 'required', 
        //     'alamat' => 'required|max:255',
        //     'image' => 'required|mimes:jpg,jpeg,png',
        // ]);



        // Post::where('id', $post->id)
        //     ->update($data);
        // return redirect('/home/posts')->with('success', 'Data Has Been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //hapus data berdasarkan id posts
        // Post::destroy($post->id);
        $post = Post::find($id);
        $post->delete();
        Alert::warning('Data Has Ben Deleted!');
        return redirect('/home/posts');
    }
}
