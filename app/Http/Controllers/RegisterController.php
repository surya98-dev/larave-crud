<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class RegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }
    
    public function store(Request $request) 
    {
        //insert data registrasi user validasi
        $data = $request->validate([
            'nik' => ['required','unique:users' ,'max:16', 'min:16'],
            'fullname' => ['required','max:255'],
            'email' => ['required','email:dns','unique:users'],
            'password' => ['required','string', Password::min(8)
                ->letters()
                ->mixedCase()
                ->numbers()
                ->uncompromised()
        ],
            // 'retype_password' => 'required|min:8|max:255'
            'retype_password' => ['required','string', Password::min(8)
                ->letters()
                ->mixedCase()
                ->numbers()
                ->uncompromised()
            ]
        ]);

        // $user = User::create([
        //     'nik' => $request->nik,
        //     'fullname' => $request->fullname,
        //     'email' => $request->email,
        //     'password' => Hash::make($request->password),
        //     'retype_password' => Hash::make($request->retype_password),
        // ]);

        // dd($request->all());

        //sembunyikan password dalam database
        // $data['password'] = bcrypt($data ['password']);
        $data['password'] = Hash::make($data['password']);
        $data['retype_password'] = Hash::make($data['retype_password']);

        //koneksi ke database dan redirect url with alert success
        User::create($data);
        // $request->session()->flash('succes' ,'Registrasi Berhasil');
        return redirect('/signin')->with('success', 'Registrasi Successfully!');
    }   
}
