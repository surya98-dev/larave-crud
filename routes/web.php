<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
// use App\Http\Controllers\AdminController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardPostController;
// use App\Http\Controllers\PostController;
use App\Http\Controllers\ForgotPasswordController;
// use App\Http\Controllers\AdsqooController;
// use App\Http\Controllers\EmailBlastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//home route
// Route::get('/admin', 'AdminController@index');
Route::get('/home', function () {
    // return view('dashboard.posts.index');
    return view('dashboard.index');
})->middleware('auth');

//test pages
Route::get('/test', function () {
    return view('profile');
})->middleware('auth');

//user route
// Route::get('/signup', 'RegisterController@index');
// Route::post('/signupstore', 'RegisterController@signupstore');
Route::get('/signup', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/signup', [RegisterController::class, 'store']);

Route::get('/signin', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/logout', [LoginController::class, 'logout']);
Route::post('/signin', [LoginController::class, 'authenticate']);

//verify email
Route::get('send-email', function () {
    Mail::to('suryafauzan199@gmail.com')
        ->send(new \App\Mail\MailtrapLogin(''));
    dd("Email Confirmation.");
});

// Auth::routes(['verify' => true]);

//profile route
Route::get('/profile', [UserController::class, 'profile']);

// // crud route
// Route::get('/pegawai', [PostController::class, 'index']);
// Route::get('/pegawai/tambah', [PostController::class, 'tambah']);
// Route::post('/pegawai/store', [PostController::class, 'store']);
// Route::get('/pegawai/edit/{id}', [PostController::class, 'edit']);
// Route::post('/pegawai/update',[PostController::class, 'update']);
// Route::get('/pegawai/hapus/{id}',[PostController::class, 'hapus']);

//resources route
Route::resource('/home/posts', DashboardPostController::class)->middleware('auth');

//forgot pwd 
// Route::get('/forgot', [ForgotPasswordController::class, 'forgot']);
// Route::post('/forgot', [ForgotPasswordController::class, 'store']);

Route::get('forget', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password');
Route::post('forget', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post');
Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');

//ADSQOO
// Route::get('/adsqoo', [AdsqooController::class, 'index'])->name('dashboard-adsqoo');
// Route::get('/adsqoo-dtp', [AdsqooController::class, 'show'])->name('dashboard-adsqoo-dtp');

//EmailBlast
// Route::get('/email-home', [EmailBlastController::class, 'index'])->name('dashboard-emailblast');
