<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Log in</title>

  <!-- Logo MDM -->
  <link rel="icon" type="image/png" href="{{ asset('dist/images/favicon.png') }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
  <!-- unhidepassword -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="hold-transition login-page">
  @include('sweetalert::alert')

<div class="login-box" action="/signin" method="post">
  <div class="login-logo">
    {{-- <a href="/signin"><b>Admin</b>LTE</a> --}}
    <a>
      <span>
      <img src="{{ asset('dist/images/Logo-MD-gray.png') }}" height="50" alt="">
      </span>
      </a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <!-- alert status -->
    @if(session()->has('succes'))
    <div class="alert alert-success alert-dismissible">
      {{ session('succes') }}
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h5><i class="icon fas fa-check">Berhasil</i></h5>
        Masukkan Email dan Password.
    </div>
    @endif

    {{-- @if(session()->has('loginError'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('loginError') }}
        <button type="button" class="btn-close" data-bs-dismis="alert" aria-label="Close"></button>
    </div>
    @endif --}}
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="/signin" method="post">
        @csrf
        <div class="input-group mb-3">
          <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" required value="{{ old('email') }}">
          @error('email')
          <div class="invalid-feedback">{{ $message }}</div>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input name="password" type="password" class="form-control" placeholder="Password" required minlength="8" id="password">
          @error('password')
          <div class="invalid-feedback">{{ $message }}</div>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <i class="fa fa-eye" aria-hidden="true" id="eye" onclick="toggle()" style="color: rgb(122, 121, 126);"></i>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="https://www.facebook.com/" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="https://accounts.google.com/" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div>
      <!-- /.social-auth-links -->
      <p class="mb-1">
        <a href="/forget">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="/signup" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- unhidepasword -->
<script>
  var state= false;
  function toggle(){
    if(state){
      document.getElementById("password").setAttribute("type","password");
      document.getElementById("eye").style.color='#7a797e';
      state = false;
    }
    else{
      document.getElementById("password").setAttribute("type","text");
      document.getElementById("eye").style.color='#5887ef';
      state = true;
    }
  }
</script>
</body>
</html>
