@extends('dashboard.layouts.main' ,['title' => 'Home'])

@section('container')
                <!-- Page Inner -->
                <div class="page-inner">
                    <nav aria-label="breadcrumb" class="mt1">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                          <li class="breadcrumb-item active"><a href="#">Email Blast</a></li>
                        </ol>
                    </nav>
                    <div class="page-title mt2">
                        <h3 class="breadcrumb-header">Hello {{  Auth::user()->fullname }}! Here's your recent email activity.</h3>
                    </div>
                    <div class="flex-center mb2">
                        <div class="form-group filter-date">
                            <label for="datepicker" class="control-label mr1">Start & End Date</label>
                            <input type="text" name="dates" id="datepicker" value="01/01/2018 - 01/15/2018" />
                        </div>
                        <div class="title-advanced" onclick="advanced()">Advanced Filter</div>
                    </div>
                    <div class="advanced-filter">
                        <div class="filter-header">
                            Search emails by all the following :
                        </div>
                        <div class="filter-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Dates</label>
                                        <select class="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Between</label>
                                        <select class="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group filter-date text-left">
                                        <label class="control-label">Range</label>
                                        <input type="text" name="dates" id="datepicker" class="d-block w-100" value="01/01/2018 - 01/15/2018" />
                                    </div>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="button" class="btn btn-default">Clear</button>
                                <button type="button" class="btn btn-default-2 primary">Search</button>
                            </div>
                        </div>
                    </div>
                    <div id="main-wrapper">
                        <div class="row auto-height">
                            <div class="col-lg-2 col-md-2 col-xs-12 cols">
                                <div class="panel panel-white stats-widget cards">
                                    <div class="panel-body">
                                        <div class="stats text-center">
                                            <h2 class="title">Requests</h2>
                                            <div class="single">414</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-12 cols">
                                <div class="panel panel-white stats-widget cards">
                                    <div class="panel-body">
                                        <div class="stats text-center">
                                            <h2 class="title">DELIVERED</h2>
                                            <div class="single text-green">99.52%</div>
                                            <div class="bottom">412</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-12 cols">
                                <div class="panel panel-white stats-widget cards">
                                    <div class="panel-body">
                                        <div class="stats text-center">
                                            <h2 class="title">OPENED</h2>
                                            <div class="single text-blue">161.35%</div>
                                            <div class="bottom">668</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-12 cols">
                                <div class="panel panel-white stats-widget cards">
                                    <div class="panel-body">
                                        <div class="stats text-center">
                                            <h2 class="title">CLICKED</h2>
                                            <div class="single text-purple">34.30%</div>
                                            <div class="bottom">142</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-12 cols">
                                <div class="panel panel-white stats-widget cards">
                                    <div class="panel-body">
                                        <div class="stats text-center">
                                            <h2 class="title">BOUNCES</h2>
                                            <div class="single text-orange">0.00%</div>
                                            <div class="bottom">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-12 cols">
                                <div class="panel panel-white stats-widget cards">
                                    <div class="panel-body">
                                        <div class="stats text-center">
                                            <h2 class="title">SPAM REPORTS</h2>
                                            <div class="single text-red">0.00%</div>
                                            <div class="bottom">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-xs-12 cols chartMe">
                                <div class="panel panel-white stats-widget cards">
                                    <div class="panel-body">
                                        <canvas id="chart11" class="mt2"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-white">
                                    <div class="panel-body">
                                        <div class="table-responsive custom-table">
                                            <table id="myTable" class="display table" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Email</th>
                                                    <th>Subject</th>
                                                    <th>Campaign</th>
                                                    <th>Sent Date</th>
                                                    <th>Last Event</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Tiger.Nixon@gmail.com</td>
                                                    <td>System Architect</td>
                                                    <td>Edinburgh</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Garrett.Winters@gmail.com</td>
                                                    <td>Accountant</td>
                                                    <td>Tokyo</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Ashton.Cox@gmail.com</td>
                                                    <td>Junior Technical Author</td>
                                                    <td>San Francisco</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Cedric.Kelly@gmail.com</td>
                                                    <td>Senior Javascript Developer</td>
                                                    <td>Edinburgh</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Airi.Satou@gmail.com</td>
                                                    <td>Accountant</td>
                                                    <td>Tokyo</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Brielle Williamson</td>
                                                    <td>Integration Specialist</td>
                                                    <td>New York</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Herrod.Chandler@gmail.com</td>
                                                    <td>Sales Assistant</td>
                                                    <td>San Francisco</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Rhona.Davidson@gmail.com</td>
                                                    <td>Integration Specialist</td>
                                                    <td>Tokyo</td>
                                                    <td>55</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Colleen.Hurst@gmail.com</td>
                                                    <td>Javascript Developer</td>
                                                    <td>San Francisco</td>
                                                    <td>32011/04/259</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Sonya.Frost@gmail.com</td>
                                                    <td>Software Engineer</td>
                                                    <td>Edinburgh</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Jena.Gaines@gmail.com</td>
                                                    <td>Office Manager</td>
                                                    <td>London</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Quinn.Flynn@gmail.com</td>
                                                    <td>Support Lead</td>
                                                    <td>Edinburgh</td>
                                                    <td>22</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Charde.Marshall@gmail.com</td>
                                                    <td>Regional Director</td>
                                                    <td>San Francisco</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Haley.Kennedy@gmail.com</td>
                                                    <td>Senior Marketing Designer</td>
                                                    <td>London</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tatyana.Fitzpatrick@gmail.com</td>
                                                    <td>Regional Director</td>
                                                    <td>London</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Michael.Silva@gmail.com</td>
                                                    <td>Marketing Designer</td>
                                                    <td>London</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Paul.Byrd@gmail.com</td>
                                                    <td>Chief Financial Officer (CFO)</td>
                                                    <td>New York</td>
                                                    <td>62011/04/254</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Gloria.Little@gmail.com</td>
                                                    <td>Systems Administrator</td>
                                                    <td>New York</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>BradleyGreer@gmail.com</td>
                                                    <td>Software Engineer</td>
                                                    <td>London</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Dai Rios@gmail.com</td>
                                                    <td>Personnel Lead</td>
                                                    <td>Edinburgh</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Jenette Caldwell@gmail.com</td>
                                                    <td>Development Lead</td>
                                                    <td>New York</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button></td>
                                                </tr>
                                                <tr>
                                                    <td>Yuri Berry@gmail.com</td>
                                                    <td>Chief Marketing Officer (CMO)</td>
                                                    <td>New York</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button></td>
                                                </tr>
                                                <tr>
                                                    <td>Caesar Vance@gmail.com</td>
                                                    <td>Pre-Sales Support</td>
                                                    <td>New York</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button></td>
                                                </tr>
                                                <tr>
                                                    <td>Doris Wilder@gmail.com</td>
                                                    <td>Sales Assistant</td>
                                                    <td>Sidney</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button></td>
                                                </tr>
                                                <tr>
                                                    <td>Angelica Ramos@gmail.com</td>
                                                    <td>Chief Executive Officer (CEO)</td>
                                                    <td>London</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button></td>
                                                </tr>
                                                <tr>
                                                    <td>Gavin Joyce@gmail.com</td>
                                                    <td>Developer</td>
                                                    <td>Edinburgh</td>
                                                    <td>2011/04/25</td>
                                                    <td>
                                                        <div class="row-details">
                                                            <strong>Event: </strong> 
                                                            <span class="active">System Architect</span>
                                                        </div>
                                                        <div class="row-details">
                                                            <strong>Date: </strong> 
                                                            <span class="active">2018/01/01 </span>
                                                        </div>
                                                    </td>
                                                    <td><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">View Detail</button></td>
                                                </tr>
                                            </tbody>
                                            </table>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Row -->
                    </div><!-- Main Wrapper -->
                </div><!-- /Page Inner -->
@endsection