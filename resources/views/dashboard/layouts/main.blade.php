<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Responsive Admin Dashboard Template">
        <meta name="keywords" content="admin,dashboard">
        <meta name="author" content="stacks">
        <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        <!-- Title -->
        <title>{{ $title ?? config('app.name') }} Blast</title>

        <!-- Styles -->
        <link rel="icon" type="image/png" href="{{ asset('dist/images/favicon.png') }}">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
        <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/icomoon/style.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/uniform/css/default.css') }}" rel="stylesheet"/>
        <link href="{{ asset('plugins/switchery/switchery.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset('plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset('plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css"/>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
      
        <!-- Theme Styles -->
        <link href="{{ asset('dist/css/space.min.css') }}" rel="stylesheet">
        <link href="{{ asset('dist/css/custom2.css') }}" rel="stylesheet">
        <link href="{{asset('dist/css/material.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="page-sidebar-fixed page-header-fixed">
@include('sweetalert::alert')
      {{-- @isset($header)
      <div style="color: red;">
         {{ $header ?? '' }}
      </div>
      @endisset
      {{ $slot }} --}}
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            @include('dashboard.layouts.sidebar')
          <!-- /Page Sidebar -->
            <!-- Page Content -->
            <div class="page-content">            
                @include('dashboard.layouts.header')
                @yield('container')
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->

        <!-- modal -->
        {{-- <div class="modal right fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog sm" role="document">
                <div class="modal-content">
                   <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel2">Detail Karyawan</h4>
                   </div>
                   <div class="modal-body">
                     @foreach ($posts as $post)
                      <div class="list-info">
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Nomor Induk Karyawan</div>
                            <div class="col-lg-8 col-md-8">: {{ $post->nik }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Nama Karyawan</div>
                            <div class="col-lg-8 col-md-8">: {{ $post->name }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Jabatan </div>
                            <div class="col-lg-8 col-md-8">: {{ $post->jabatan }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Umur </div>
                            <div class="col-lg-8 col-md-8">: {{ $post->umur }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Alamat Karyawan</div>
                            <div class="col-lg-8 col-md-8">: {{ $post->alamat }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Foto Karyawan</div>
                            <div class="col-lg-8 col-md-8">: {{ $post->image }}</div>
                         </div>
                      </div>
                      @endforeach
                   </div>
                </div>
                <!-- modal-content -->
             </div>
        </div>  --}}
        {{-- <div class="modal right fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        @include('dashboard.layouts.modals')
        </div> --}}
        <!-- /modal -->

        <!-- Javascripts -->
        <script src="{{ asset('plugins/jquery/jquery-3.1.0.min.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('plugins/uniform/js/jquery.uniform.standalone.js') }}"></script>
        <script src="{{ asset('plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ asset('plugins/chartjs/chart.min.js') }}"></script>
        <script src="{{ asset('plugins/d3/d3.min.js') }}"></script>
        <script src="{{ asset('plugins/nvd3/nv.d3.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.time.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.symbol.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.resize.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.pie.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/js/jquery.datatables.min.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('dist/js/space.min.js') }}"></script>
        <script src="{{ asset('dist/js/pages/chart.js') }}"></script>
        <script src="{{ asset('dist/js/pages/table-data.js') }}"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
        {{-- <script src="https://code.jquery.com/jquery-3.6.0.slim.js" integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script> --}}
        <script>
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
            $('#myTable').DataTable( {
                responsive: true
            } );
        </script>
         <script>
                  $(document).ready(function() {
                      "use strict";
                      new Chart(document.getElementById("chart11"),{"type":"line","data":{"labels":["11","12","13","14","15"],"datasets":[{"label":"","data":[65,59,80,81,56],"fill":false,"borderColor":"rgb(99, 203, 137)","lineTension":0.1}]},"options":{maintainAspectRatio: false,responsive: true}});
                      
                      window.onresize = function(event) {
                          if($(window).width() < 410 ){
                              $('#menu1').removeClass('page-sidebar-collapsed')
                          }
                      };
                  });
                  // $('#myTable').DataTable( {
                  //     responsive: true
                  // });
                  $('input[name="dates"]').daterangepicker({
                      ranges: {
                      'Today': [moment(), moment()],
                      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                      'This Month': [moment().startOf('month'), moment().endOf('month')],
                      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                      }
                  });
      
                  function advanced(){
                      $('.advanced-filter').toggle()
                  }
        </script>
        {{-- <script>
            swal("Hello {{  Auth::user()->fullname }}", "Have a Nice Day 😁");
        </script> --}}
        {{-- <script>
            $('.delete').click( function(){
                swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this imaginary file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
        swal("Poof! Your imaginary file has been deleted!", {
        icon: "success",
        });
        } else {
        swal("Your imaginary file is safe!");
        }
        });
            });
        </script>
        <script>
            $('.create').click(function(){
                swal("Data Berhasil Ditambahkan");
            });
        </script> --}}
    </body>
</html>