            <!-- Page Header -->
            <div class="page-header">
                
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <div class="logo-sm">
                                <a href="javascript:void(0)" id="sidebar-toggle-button"><i class="fa fa-bars"></i></a>
                                <a class="logo-box" href="/home/posts"><img src="{{ asset('dist/images/yp-bymd.png') }}" height="50" alt=""></a>
                            </div>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <i class="fa fa-angle-down"></i>
                            </button>
                        </div>
                    
                        <!-- Collect the nav links, forms, and other content for toggling -->
                    
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="logo-full head-nav">
                                  <a href="/home/posts">
                                  <span>
                                  <img src="{{ asset('dist/images/yp-bymd.png') }}" height="50" alt="">
                                  </span>
                                  </a>
                                  <a href="javascript:void(0)" id="collapsed-sidebar-toggle-button">
                                  <i class="fa fa-bars"></i>
                                  </a>
                                </li>
                                <li class="head-nav actived">
                                  <a href="#">
                                  <img src="{{ asset('dist/images/email.svg') }}" alt="" height="20" class="logo-dtp">
                                  <span>Email Blast</span>
                                  </a>
                                </li>
                                <li class="head-nav">
                                  <a href="#">
                                  <img src="{{ asset('dist/images/gray-icon-blast.svg') }}" alt="" height="20" class="logo-dtp">
                                  <span>SMS Blast</span>
                                  </a>
                                </li>
                                <li class="head-nav">
                                  <a href="#">
                                  <img src="{{ asset('dist/images/chat.svg') }}" alt="" height="20" class="logo-dtp">
                                  <span>WA Blast</span>
                                  </a>
                                </li>
                                <li class="head-nav">
                                  <a href="adsqoo-dashboard.html">
                                  <img src="{{ asset('dist/images/gray-icon-adsQoo.svg') }}" alt="" height="20" class="logo-dtp adsqoo">
                                  <span>ADSQOO</span>
                                  </a>
                                </li>
                                <li class="head-nav">
                                  <a href="adsqoo-dashboard.html">
                                    <img src="{{ asset('dist/images/gray-icon-yp.png') }}" alt="" height="20" class="logo-dtp adsqoo">
                                    <span>Yellowpages</span>
                                  </a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
                                    <ul class="dropdown-menu dropdown-lg dropdown-content">
                                        <li class="drop-title">Notifications<a href="notification.html" class="drop-title-link">Lihat semua <i class="fa fa-angle-right ml0h"></i></a></li>
                                        <li class="slimscroll dropdown-notifications">
                                            <ul class="list-unstyled dropdown-oc">
                                                <li>
                                                    <a href="#"><span class="notification-badge bg-primary"><i class="fa fa-photo"></i></span>
                                                        <span class="notification-info">Finished uploading photos to gallery <b>"South Africa"</b>.
                                                            <small class="notification-date">20:00</small>
                                                        </span></a>
                                                </li>
                                                <li>
                                                    <a href="#"><span class="notification-badge bg-primary"><i class="fa fa-at"></i></span>
                                                        <span class="notification-info"><b>John Doe</b> mentioned you in a post "Update v1.5".
                                                            <small class="notification-date">06:07</small>
                                                        </span></a>
                                                </li>
                                                <li>
                                                    <a href="#"><span class="notification-badge bg-danger"><i class="fa fa-bolt"></i></span>
                                                        <span class="notification-info">4 new special offers from the apps you follow!
                                                            <small class="notification-date">Yesterday</small>
                                                        </span></a>
                                                </li>
                                                <li>
                                                    <a href="#"><span class="notification-badge bg-success"><i class="fa fa-bullhorn"></i></span>
                                                        <span class="notification-info">There is a meeting with <b>Ethan</b> in 15 minutes!
                                                            <small class="notification-date">Yesterday</small>
                                                        </span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown user-dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('dist/images/user.svg') }}" height="20" alt="" class="account"></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/profile">Account Settings</a></li>
                                        <li><a class="dopdown-item" href="/logout" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>
                                            <form id="logout-form" action="/logout" method="post" class="d-none">
                                                @csrf
                                            </form>  
                                        <li>
                                            {{-- @auth
                                        <form action="/logout" method="POST">
                                             @csrf
                                                <button type="button" class="dropdown-item">Logout</button>
                                            </form>  
                                            @else
                                            <a href="/signin" class="nav-link {{ ($active === "signin") ? 'active' : '' }}">
                                                <span>LOGIN</span>
                                            </a>
                                            @endauth --}}
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div><!-- /Page Header -->
