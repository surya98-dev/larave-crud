       <!-- Page Sidebar -->
       <div class="page-sidebar">
        <div class="page-sidebar-inner">
            <div class="page-sidebar-menu">
                <ul class="accordion-menu">
                    <li>
                        <a href="/home" class="active">
                            <i class="menu-icon icon-home4"></i><span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                        <i class="menu-icon icon-envelop"></i>
                        <span>Email Template</span>
                            <i class="accordion-icon fa fa-angle-left"></i>
                        </a>
                        <ul class="sub-menu">
                            <li><a href="/home/posts">List</a></li>
                            {{-- <li>
                                <a href="/home/posts/create">
                                   <span>Create</span>
                                </a>
                            </li> --}}
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                        <i class="menu-icon icon-book"></i>
                        <span>Subscriber Mgt</span>
                            <i class="accordion-icon fa fa-angle-left"></i>
                        </a>
                        <ul class="sub-menu">
                            <li><a href="manage-contact.html">Manage Contact</a></li>
                            <li><a href="#">Manage Segment</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="menu-icon icon-newspaper"></i><span>Single Sends</span>
                        </a>
                    </li>
                    <li>
                        <a href="assign-sender.html">
                            <i class="menu-icon icon-compass"></i><span>Assign Sender</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="menu-icon icon-bullhorn"></i>
                            <span>Broadcast</span>
                            <i class="accordion-icon fa fa-angle-left"></i>
                        </a>
                        <ul class="sub-menu">
                            <li><a href="broadcast.html">Broadcast</a></li>
                            <li><a href="multicast.html">Multicast</a></li>
                            <li><a href="dynamic.html">Dynamic SMS</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="menu-icon icon-file-zip"></i>
                                <span>Report</span>
                            <i class="accordion-icon fa fa-angle-left"></i>
                        </a>
                        <ul class="sub-menu">
                            <li><a href="summary-report.html">Summary Report</a></li>
                            <li><a href="traffic-report.html">Traffic Report</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="menu-icon icon-credit-card"></i>
                                <span>Billing</span>
                            <i class="accordion-icon fa fa-angle-left"></i>
                        </a>
                        <ul class="sub-menu">
                            <li><a href="billing-topup.html">Top Up</a></li>
                            <li><a href="payment-history.html">Payment History</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="menu-icon icon-feed"></i>
                                <span>Campaign</span>
                            <i class="accordion-icon fa fa-angle-left"></i>
                        </a>
                        <ul class="sub-menu">
                            <li><a href="detail-dooh.html">DOOH</a></li>
                            <li><a href="detail-sms.html">SMS Blast</a></li>
                            <li><a href="detail-lba.html">SMS LBA</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="menu-icon icon-file-text"></i><span>Applications</span>
                        </a>
                    </li>
                    <li>
                        <a href="team-members.html">
                            <i class="menu-icon icon-users"></i><span>Teams</span>
                        </a>
                    </li>
                    <li>
                        <a href="pricing.html">
                            <i class="menu-icon icon-coin-dollar"></i><span>Pricing</span>
                        </a>
                    </li>
                    <li>
                                                    <a href="financial-report.html">
                                                        <i class="menu-icon icon-libreoffice"></i><span>Financial Report</span>
                                                    </a>
                                                </li>
                    <li>
                                                    <a href="fb-ads.html">
                                                        <i class="menu-icon icon-facebook2"></i><span>FB Ads</span>
                                                    </a>
                                                </li> 
                </ul>
            </div>
        </div>
    </div><!-- /Page Sidebar -->