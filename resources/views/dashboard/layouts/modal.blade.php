            <div class="modal-dialog sm" role="document">
                <div class="modal-content">
                   <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel2">Detail Karyawan</h4>
                   </div>
                   <div class="modal-body">
                     {{-- @foreach ($posts as $post) --}}
                      <div class="list-info">
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Nomor Induk Karyawan</div>
                            <div class="col-lg-8 col-md-8">: {{ $post->nik }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Nama Karyawan</div>
                            <div class="col-lg-8 col-md-8">: {{ $post->name }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Jabatan </div>
                            <div class="col-lg-8 col-md-8">: {{ $post->jabatan }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Umur </div>
                            <div class="col-lg-8 col-md-8">: {{ $post->umur }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Alamat Karyawan</div>
                            <div class="col-lg-8 col-md-8">: {{ $post->alamat }}</div>
                         </div>
                         <div class="row info-detail">
                            <div class="col-lg-4 col-md-4">Foto Karyawan</div>
                            <div class="col-lg-8 col-md-8">: {{ $post->image }}</div>
                         </div>
                      </div>
                      {{-- @endforeach --}}
                   </div>
                </div>
                <!-- modal-content -->
             </div>