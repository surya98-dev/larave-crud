@extends('dashboard.layouts.main',['title' => 'Edit'])

@section('container')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Form Tambah Pegawai</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

            <!-- general form elements disabled -->
        <div class="card card-warning">
                {{-- <div class="card-header">
                  <h3 class="card-title">Update Data Pegawai</h3>
                </div> --}}
                <!-- /.card-header -->
                <div class="card-body">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                          @foreach ($errors->all () as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                      </ul>
                    </div>
                    @endif
                  <form method="post" action="{{ route('posts.update', $post->id) }}" enctype="multipart/form-data">
                    @csrf
                    {{-- {{ csrf_field() }} --}}
                    @method('PUT')
                    {{-- <div class="row">
                      <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                          <label> NIK Pegawai</label>
                          <input type="number" class="form-control" placeholder="NIK Pegawai" name="nik" required autofocus value="{{ $post->nik }}">
                        </div>
                        <div class="form-group">
                          <label>Nama Pegawai</label>
                          <input type="text" class="form-control" placeholder="Nama Lengkap Pegawai" name="name" required autofocus value="{{ $post->name }}">
                        </div>
                        <div class="form-group">
                          <label>Jenis Kelamin</label>
                          <input type="text" class="form-control" placeholder="Jenis Kelamin" name="jeniskelamin" required autofocus value="{{ $post->jeniskelamin }}">
                        </div>
                        <div class="form-group">
                            <!-- select -->
                            <div class="form-group">
                              <label>Jabatan Pegawai</label>
                              <select class="form-control" name="jabatan" required autofocus value="{{ $post->jabatan }}">
                                <option>Programmer</option>
                                <option>IT</option>
                                <option>Networking</option>
                              </select>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="umur" class="bmd-label-floating">Umur</label>
                            <input type="number" class="form-control" id="umur" name="umur" required autofocus value="{{ $post->umur }}">
                        </div>
                        <div class="form-group">
                          <label>Alamat Lengkap</label>
                          <input type="text" class="form-control" placeholder="Alamat Lengkap Pegawai" name="alamat" required autofocus value="{{ $post->alamat }}">
                        </div>
                      </div>
                    </div>
                    <!-- input states -->
                    <button type="submit" class="btn btn-primary">Simpan Data Pegawai</button> 
                    <a href="{{ route('posts.index') }}" class="btn btn-success"><span data-feather="arrow-left"></span>Back To Data Pegawai</a> --}}

                    <div class="page-title mt2 mb1">
                      <h3 class="breadcrumb-header">Update Data Pegawai</h3>
                    </div>
                    <div id="main-wrapper">
                      <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12">
                          <!-- panel -->
                          <div class="panel panel-white stats-widget cards">
                            <div class="panel-body">
                              <div class="row">
                                {{-- <input type="hidden" name="id" id="id" value="{{ $post->id }}"> --}}
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group normal filter-date">
                                    <label>Nik Pegawai</label>
                                    {{-- <input type="number" class="form-control" placeholder="NIK Pegawai" name="nik" required autofocus value="{{ $post->nik }}"> --}}
                                    <input class="form-control" type="number" placeholder="NIK Pegawai" aria-label="Disabled input example" disabled value="{{ $post->nik }}">
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group normal filter-date">
                                    <label>Nama Pegawai</label>
                                    <input type="text" class="form-control" placeholder="Nama Lengkap Pegawai" name="name" required autofocus value="{{ $post->name }}">
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>Jenis Kelamin</label>
                                    <select class="form-control" name="jeniskelamin" required autofocus value="{{ $post->jeniskelamin }}">
                                      <option>Cowo</option>
                                      <option>Cewe</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>No Handphone</label>
                                    <input type="text" class="form-control" placeholder="No Handphone" name="nohp" required autofocus value="{{ $post->nohp }}">
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>Umur</label>
                                    <input type="text" class="form-control" placeholder="Umur" name="umur" required autofocus value="{{ $post->umur }}">
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>Jabatan</label>
                                    <select class="form-control" name="jabatan" required autofocus value="{{ $post->jabatan }}">
                                      <option>Programmer</option>
                                      <option>IT</option>
                                      <option>Engginer</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>Alamat</label>
                                    <input type="text" class="form-control" placeholder="Alamat" name="alamat" required autofocus value="{{ $post->alamat }}">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- panel -->
                          <div class="text-center topup-empty">
                            <div class="panel panel-white stats-widget cards">
                              <label for="image">Foto Karyawan</label>
                              <div class="panel-body">
                                <img src="{{ asset('storage/' . $post->image) }}" width="300px" height="250px" class="img-fluid mb1 mt2"/>
                                <div>
                                  <p class="text-muted">{{ $post->created_at->format('Y-M-d') }} &middot; {{ $post->created_at->diffForHumans() }}</p>
                                </div>
                              </div>
                              <br>
                              <input type="file" class="from-group" id="image" name="image">
                            </div>
                          </div>
                          <button type="submit" class="btn btn-default-2 btn-addon mt2"><i class="fa fa-save"></i>Update</button>
                          {{-- <button type="submit" class="btn btn-primary">Simpan Data Pegawai</button>  --}}
                        </div>
                      </div>
                      {{-- <button type="submit" class="btn btn-default-2 btn-addon mt2"><i class="fa fa-save"></i>Update</button> --}}
                    </div>
                  </form>
                </div>
                <!-- /.card-body -->
              </div>
@endsection