@extends('dashboard.layouts.main',['title' => 'Show'])

@section('container')

            <!-- general form elements disabled -->
            <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Read Only Details Pegawai</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form>
                  {{-- <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>NIK Pegawai</label>
                        <input type="text" class="form-control" disabled placeholder="{{ $posts->nik }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Nama Pegawai</label>
                        <input type="text" class="form-control" disabled placeholder="{{ $posts->name }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Jenis Kelamin</label>
                      <input type="text" class="form-control" disabled placeholder="{{ $posts->jeniskelamin }}">
                    </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>No Handphone</label>
                      <input type="text" class="form-control" disabled placeholder="{{ $posts->nohp }}">
                    </div>
                  </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Umur Pegawai</label>
                        <input type="text" class="form-control" disabled placeholder="{{ $posts->umur }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Jabatan Pegawai</label>
                        <input type="text" class="form-control" disabled placeholder="{{ $posts->jabatan }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Alamat Pegawai</label>
                        <input type="text" class="form-control"  disabled placeholder="{{ $posts->alamat }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Foto Kayrawan</label>
                        <div>
                        <img width="125px" height="75px" src="{{ asset('storage/' . $posts->image) }}" alt="default" style="object-fit: cover;">
                        <p class="text-muted">{{ $posts->created_at->format('Y-M-d') }} &middot; {{ $posts->created_at->diffForHumans() }}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="d-flex">
                <a href="{{ route('posts.index') }}" class="btn btn-success"><span data-feather="arrow-left"></span>Back To Data Pegawai</a>
                <a href="{{ route('posts.edit', $posts->id) }}" class="btn btn-warning"><span data-feather="edit"></span>Edit Data Pegawai</a>
                </div> --}}


                <div class="col-lg-6 col-md-6 col-xs-12 cols">
                  <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="row flex-center">
                          <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="title-default">Detail Pegawai</div>
                            <br>
                            <div class="list-info no-border">
                              <div class="row info-detail">
                                  <div class="col-lg-5 col-md-5 left">NIK Pegawai</div>
                                  <div class="col-lg-6 col-md-6">: {{ $posts->nik }}</div>
                              </div>
                              <div class="row info-detail">
                                  <div class="col-lg-5 col-md-5 left">Name Pegawai</div>
                                  <div class="col-lg-6 col-md-6">: {{ $posts->name }}</div>
                              </div>
                              <div class="row info-detail">
                                  <div class="col-lg-5 col-md-5 left">Jenis Kelamin</div>
                                  <div class="col-lg-6 col-md-6">: {{ $posts->jeniskelamin }}</div>
                              </div>
                              <div class="row info-detail">
                                  <div class="col-lg-5 col-md-5 left">No Handphone</div>
                                  <div class="col-lg-6 col-md-6">: {{ $posts->nohp }}</div>
                              </div>
                              <div class="row info-detail">
                                  <div class="col-lg-5 col-md-5 left">Umur</div>
                                  <div class="col-lg-6 col-md-6">: {{ $posts->umur }}</div>
                              </div>
                              <div class="row info-detail">
                                  <div class="col-lg-5 col-md-5 left">Jabatan</div>
                                  <div class="col-lg-6 col-md-6">: {{ $posts->jabatan }}</div>
                              </div>
                              <div class="row info-detail">
                                  <div class="col-lg-5 col-md-5 left">Alamat</div>
                                  <div class="col-lg-6 col-md-6">: {{ $posts->alamat }}</div>
                              </div>
                              {{-- <div class="row"> --}}
                                  {{-- <div class="col-lg-5 col-md-5 left">Foto Pegawai</div>
                                  <div class="col-lg-6 col-md-6">
                                    <img width="125px" height="75px" src="{{ asset('storage/' . $posts->image) }}" alt="default" style="object-fit: cover;">
                                    <p class="text-muted">{{ $posts->created_at->format('Y-M-d') }} &middot; {{ $posts->created_at->diffForHumans() }}</p>
                                  </div> --}}
                                  <div class="panel panel-white">
                                    <div class="panel-body">
                                      <div class="title-default mb2">Foto Karyawan</div>
                                      <iframe width="250px" height="200px" src="{{ asset('storage/' . $posts->image) }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                      <p class="text-muted">{{ $posts->created_at->format('Y-M-d') }} &middot; {{ $posts->created_at->diffForHumans() }}</p>
                                    </div>
                                  </div>
                              {{-- </div> --}}
                              <div class="row">
                                <div class="col-sm-12">
                                  <ul class="pager wizard no-margin">
                                    <li class="next" data-target="#step2">
                                      <a href="{{ route('posts.index') }}" class="btn btn-default-2 primary"><span data-feather="arrow-left"></span>Home</a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                </form>
                {{-- <div class=""> --}}
                {{-- <form action="{{ route('posts.destroy', $posts->id) }}" method="POST" class="d-inline">
                  @method('delete')
                  @csrf
                  <button class="btn btn-danger" onclick="return confirm('Delete data?')"><span data-feather="x-circle"></span>Delete Data Pegawai</button>
                </form> --}}
                {{-- </div> --}}
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- general form elements disabled -->

@endsection