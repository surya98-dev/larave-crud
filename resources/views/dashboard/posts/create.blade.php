@extends('dashboard.layouts.main',['title' => 'Create'])

@section('container')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Form Tambah Pegawai</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

            <!-- general form elements disabled -->
            <div class="card card-warning">
              <div class="page-title ml1 mt2 mb1">
                <h3 class="breadcrumb-header">Tambah Pegawai Baru</h3>
              </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <form method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">
                    @csrf
                    {{-- <div class="row">
                      <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                          <label> NIK Pegawai</label>
                          <input type="number" class="form-control @error('nik') is-invalid @enderror" placeholder="NIK Pegawai" name="nik" required value="{{ old('nik') }}">
                          @error('nik')
                          <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                        <div class="form-group">
                          <label>Nama Pegawai</label>
                          <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nama Lengkap Pegawai" name="name" required value="{{ old('name') }}">
                          @error('name')
                          <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                        <div class="form-group">
                          <!-- select -->
                          <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <select class="form-control" name="jeniskelamin">
                              <option>Cowo</option>
                              <option>Cewe</option>
                            </select>
                        </div>
                        </div>
                        <div class="form-group">
                        <label for="nohp" class="bmd-label-floating">Nomor Handphone</label>
                        <input type="number" class="form-control @error('nohp') is-invalid @enderror" id="nohp" name="nohp" required value="{{ old('nohp') }}">
                        @error('nohp')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="form-group">
                            <!-- select -->
                            <div class="form-group">
                              <label>Jabatan Pegawai</label>
                              <select class="form-control" name="jabatan">
                                <option>Programmer</option>
                                <option>IT</option>
                                <option>Networking</option>
                              </select>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="umur" class="bmd-label-floating">Umur</label>
                            <input type="number" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" required value="{{ old('umur') }}">
                            @error('umur')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                          <label>Alamat Lengkap</label>
                          <input type="text" class="form-control @error('alamat') is-invalid @enderror" placeholder="Alamat Lengkap Pegawai" name="alamat" required value="{{ old('alamat') }}">
                          @error('alamat')
                          <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                        <div class="form-group">
                          <label for="image">Choose Image</label>
                          <br>
                          <input type="file" class="from-control @error('image') is-invalid @enderror" id="image" name="image">
                          @error('image')
                          <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>
                    </div> --}}
                         <!-- input states -->
                    {{-- <button type="submit" class="btn btn-primary">Create Pegawai</button> --}}
                    {{-- <a href="{{ route('posts.index') }}" class="btn btn-success"><span data-feather="arrow-left"></span>Back To Data Pegawai</a> --}}


                    {{-- updated frondend --}}
                    <div id="main-wrapper">
                      <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12">
                          <!-- panel -->
                          <div class="panel panel-white stats-widget cards">
                            <div class="panel-body">
                              <div class="row">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                  <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                  <ul>
                                      @foreach ($errors->all () as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                  </ul>
                                </div>
                                @endif
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group normal filter-date">
                                    <label>Nik Pegawai</label>
                                    <input type="number" class="form-control @error('nik') is-invalid @enderror" placeholder="NIK Pegawai" name="nik" required value="{{ old('nik') }}">
                                    @error('nik')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group normal filter-date">
                                    <label>Nama Pegawai</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nama Lengkap Pegawai" name="name" required value="{{ old('name') }}">
                                    @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>Jenis Kelamin</label>
                                    <select class="form-control" name="jeniskelamin" required  value="{{ old('jeniskelamin') }}">
                                      <option>Cowo</option>
                                      <option>Cewe</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>No Handphone</label>
                                    <input type="text" class="form-control @error('nohp') is-invalid @enderror" placeholder="No Handphone" name="nohp" required  value="{{ old('nohp') }}">
                                    @error('nohp')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>Umur</label>
                                    <input type="text" class="form-control @error ('umur') is-invalid @enderror" placeholder="Umur" name="umur" required  value="{{ old('umur') }}">
                                    @error('umur')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>Jabatan</label>
                                    <select class="form-control" name="jabatan" required  value="{{ old('jabatan') }}">
                                      <option>Programmer</option>
                                      <option>IT</option>
                                      <option>Engginer</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                  <div class="form-group field-map d-block">
                                    <label>Alamat</label>
                                    <input type="text" class="form-control @error ('alamat') is-invalid @enderror" placeholder="Alamat Lengkap" name="alamat" required  value="{{ old('alamat') }}">
                                    @error('alamat')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- panel -->
                          <div class="form-group">
                            <label for="image">Choose Image</label>
                            <br>
                            <input type="file" class="from-control @error('image') is-invalid @enderror" id="image" name="image">
                            @error('image')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                          </div>
                          <button type="submit" class="btn btn-default-2 btn-addon mt2 create"><i class="fa fa-save"></i>Create</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.card-body -->
              </div>
@endsection