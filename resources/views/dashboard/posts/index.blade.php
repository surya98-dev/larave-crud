@extends('dashboard.layouts.main' ,['title' => 'Home'])

@section('container')
           <!-- Page Inner --> 
            <div class="page-inner">
                <nav aria-label="breadcrumb" class="mt1">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                      <li class="breadcrumb-item active"><a href="#">Single Sends</a></li>
                    </ol>
                </nav>
                <div class="page-title mt2 mb1">
                    <h3 class="breadcrumb-header">Single Sends</h3>
                </div>
                {{-- @if(session()->has('success'))
                <div class="alert alert-success" role="alert">
                  {{ session('success') }}
                </div>
                @endif --}}
                <p class="mb2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>

                <div class="flex-center">
                    <div>
                        <a href="{{ route('posts.create') }}" class="btn btn-default-2 btn-addon"><i class="fa fa-plus"></i>Single Sends</a>
                    </div>
                </div>
                <div id="main-wrapper">
                    <div class="row mt2">
                        <div class="col-lg-12 col-md-12 cols">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="table-responsive have-dropdown custom-table">
                                        <table id="myTable" class="display table" style="width: 100%; cellspacing: 0;">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>NIK</th>
                                                    <th>Name Karyawan</th>
                                                    <th>Jabatan</th>
                                                    <th width="150">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              @foreach ($posts as $post)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $post->nik }}</td>
                                                    <td>{{ $post->name }}</td>
                                                    <td>{{ $post->jabatan }}</td>
                                                    {{-- <td>{{ $post->umur }}</td> --}}
                                                    {{-- <td>{{ $post->alamat }}</td> --}}
                                                    {{-- <td>{{ $post->image }}</td> --}}
                                                    <td class="flex-center pos-relative">
                                                        {{-- <a href="" class="btn btn-default btn-addon" data-toggle="modal" data-target="#myModal4"><i class="fa fa-gear"></i>Manage</a>
                                                        <button type="button" class="btn btn-default more dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                                        <div class="dropdown-menu custom">
                                                            <a class="dropdown-item" href="/home/posts/{{ $post->id }}/edit">Edit</a>
                                                            <a class="dropdown-item" href="#" >
                                                              <form action="/home/posts/{{ $post->id }}" method="post">
                                                                @method('delete')
                                                                @csrf
                                                                <h5 onclick="return confirm('Delete data?')">Remove</h5>
                                                              </form>
                                                            </a>
                                                        </div> --}}
                                                        <div class="input-group-prepend">
                                                          <button type="button" class="btn btn-default btn-addon dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-gear"></i>
                                                            Manage
                                                          </button>
                                                          <ul class="dropdown-menu">
                                                            {{-- <li class="dropdown-item"><a data-toggle="modal" data-target="#myModal4">Details</a></li> --}}
                                                            <li class="dropdown-item"><a class="dropdown-item" href="{{ route('posts.show', $post->id) }}">Details</a></li>
                                                            <li class="dropdown-item"><a class="dropdown-item" href="{{ route('posts.edit', $post->id) }}">Modify</a></li>
                                                          </ul>
                                                        </div>
                                                        <div class="input-group-prepend">
                                                            <form action="{{ route('posts.destroy', $post->id) }}" method="POST" class="d-inline delete">
                                                                @method('delete')
                                                                @csrf
                                                                <button class="btn btn-default more" oneclick="question"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        </div>
                                                        <!-- /btn-group -->
                                                    </td> 
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
            </div><!-- /Page Inner -->
@endsection