@extends('layout.iofrm.main')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Responsive Admin Dashboard Template">
        <meta name="keywords" content="admin,dashboard">
        <meta name="author" content="stacks">
        <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        <!-- Title -->
        <title>Email Blast</title>

        <!-- Styles -->
        <link rel="icon" type="image/png" href="./assets/images/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
        <link href="./assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="./assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="./assets/plugins/icomoon/style.css" rel="stylesheet">
      
        <!-- Theme Styles -->
        <link href="./assets/css/space.css" rel="stylesheet">
        <link href="./assets/css/custom2.css" rel="stylesheet">
        <link href="./assets/css/material.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="form-body error-404">
            <div class="form-content text-center">
                <img src="./assets/images/403.png" width="500" class="img-fluid"/>
                <h2 class="mt2">Error Forbidden</h2>
                <p class="text-center">We can't seem to find the page you're looking for.</p>
                <div class="form-button">
                    <button type="button" class="btn btn-default btn-addon mt2">Back to Home</button>
                </div>
            </div>
        </div>
    </body>
</html>
@endsection