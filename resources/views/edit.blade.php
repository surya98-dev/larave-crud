<!DOCTYPE html>
<html>
<head>
	<title>MY_CRUD</title>
</head>
<body>

	<h3>Edit Pegawai</h3>

	<a href="/pegawai"> Kembali</a>
	
	<br/>
	<br/>

	@foreach($pegawai as $p)
	<form action="/pegawai/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $p->id }}"> <br/>
		nik <input type="number" required="required" name="nik" value="{{ $p->nik }}"> <br/>
		Nama <input type="text" required="required" name="name" value="{{ $p->name }}"> <br/>
		Jabatan <input type="text" required="required" name="jabatan" value="{{ $p->jabatan }}"> <br/>
		Umur <input type="number" required="required" name="umur" value="{{ $p->umur }}"> <br/>
		Alamat <textarea required="required" name="alamat">{{ $p->alamat }}</textarea> <br/>
		<input type="submit" value="Simpan Data">
	</form>
	@endforeach
		

</body>
</html>