@extends('dashboard.layouts.main',['title' => 'Profile'])

@section('container')
      <!-- Page Inner -->
      <div class="page-inner">
        <nav aria-label="breadcrumb" class="mt1">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
              <li class="breadcrumb-item active"><a href="#">Account Settings</a></li>
            </ol>
        </nav>
        <div class="page-title mt2 mb1">
            <h3 class="breadcrumb-header">Account Settings</h3>
        </div>
        
        <div id="main-wrapper">
            <div class="row mt2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="title-default mb2">User Profile</div>
                            <div class="user-profile row">
                                <div class="col-md-6">
                                    <div class="pic-profile">
                                        <img src="{{ asset('dist/images/avatar2.png') }}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="info-profile">
                                        <div class="material-form-field">
                                            <input type="text" required name="text" id="nem" value="Ferry" readonly/>
                                            <label class="material-form-field-label" for="nem">First Name</label>
                                        </div>
                                        <div class="material-form-field">
                                            <input type="text" required name="text" id="description" value="Ardhana"/>
                                            <label class="material-form-field-label" for="description">Last Name</label>
                                        </div>
                                        <div class="material-form-field">
                                            <input type="text" required name="text" id="email" value="ferry.ardhana@gmail.com"/>
                                            <label class="material-form-field-label" for="email">Email</label>
                                        </div>
                                        <div class="material-form-field">
                                            <input type="text" required name="text" id="address" />
                                            <label class="material-form-field-label" for="address">Alamat Lengkap</label>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-default-2 btn-addon mt2"><i class="fa fa-save"></i>Update User Profile</button>
                                </div>
                            </div>
                            <hr>
                            <div class="title-default mb2">Phone Number</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>You will need this number to access two factor authentication enabled accounts.</p>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="material-form-field">
                                        <input type="text" required name="text" id="email" />
                                        <label class="material-form-field-label" for="email">Phone Number</label>
                                    </div>
                                    <button type="button" class="btn btn-default-2 btn-addon mt2"><i class="fa fa-plus"></i>Add Phone Number</button>
                                </div>
                            </div>
                            <hr>
                            <div class="title-default mb2">Setup Two-Factor Authentication(2FA)</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Two-factor authentication (2FA) requires users to enter a one-time verification code sent using your preferred channel in order to access a DTP account.</p>
                                </div>
                                <div class="col-md-6">
                                    <div class="fa-method">
                                        <div class="flex-center">
                                            <img src="{{asset('dist/images/phone-sms.svg')}}" alt="" width="40">
                                            <p class="mb0 ml1">Melalui <b>SMS</b> ke ****8912</p>
                                        </div>
                                        <i class="fa fa-chevron-right"></i>
                                    </div>
                                    <div class="fa-method">
                                        <div class="flex-center">
                                            <img src="{{asset('dist/images/phone-wa.svg')}}" alt="" width="40">
                                            <p class="mb0 ml1">Melalui <b>Whatsapp</b> ke ****8912</p>
                                        </div>
                                        <i class="fa fa-chevron-right"></i>
                                    </div>
                                    <div class="fa-method">
                                        <div class="flex-center">
                                            <img src="{{ asset('dist/images/phone-mail.svg') }}" alt="" width="40">
                                            <p class="mb0 ml1">Melalui <b>Email</b> ke ferry.ardhana@gmail.com</p>
                                        </div>
                                        <i class="fa fa-chevron-right"></i>
                                    </div>
                                    <button type="button" class="btn btn-default-2 btn-addon" data-toggle="modal" data-target="#myModal3"><i class="fa fa-plus"></i>Add 2FA Setting</button>
                                </div>
                            </div>
                            <hr>
                            <div class="title-default mb2">Change Password</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="material-form-field pos-relative">
                                        <input type="text" required name="text" id="current">
                                        <label class="material-form-field-label" for="current">Current Password</label>
                                        <i class="fa fa-eye show-pass"></i>
                                    </div>
                                    <div class="material-form-field">
                                        <input type="text" required name="text" id="new">
                                        <label class="material-form-field-label" for="new">New Password</label>
                                        <i class="fa fa-eye show-pass"></i>
                                    </div>
                                    <div class="material-form-field">
                                        <input type="text" required name="text" id="retype">
                                        <label class="material-form-field-label" for="retype">Retype New Password</label>
                                        <i class="fa fa-eye show-pass"></i>
                                    </div>
                                    <button type="button" class="btn btn-default-2 btn-addon mt2"><i class="fa fa-save"></i>Save New Password</button>
                                </div>
                            </div>
                            <hr>
                            <div class="title-default mb2">Document</div>
                            <div class="row">
                              <div class="col-md-12 mb2">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                              </div>
                              <div class="col-md-6">
                                <p class="title fw-400">NPWP</p>
                                <div class="material-form-field pos-relative">
                                  <input type="text" required name="text" id="current">
                                  <label class="material-form-field-label" for="current">No NPWP</label>
                                </div>
                                <label class="mt1">Upload Photo NPWP</label>
                                <div class="dropzone-wrapper mt0">
                                  <div class="dropzone-desc">
                                      <div class="col-md-4">
                                        <img src="{{ asset('dist/images/upload.svg') }}" alt="" srcset="" width="50px">
                                      </div>
                                      <div class="col-md-6">
                                        <h5 style="font-weight: 700;">Drop your file here, or browse</h5>
                                        <small class="info f-grey">Support : .png, .jpg, .jpeg</small>
                                      </div>
                                  </div>
                                  <input type="file" name="img_logo" class="dropzone">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <p class="title fw-400">KTP</p>
                                <div class="material-form-field pos-relative">
                                  <input type="text" required name="text" id="current">
                                  <label class="material-form-field-label" for="current">No NPWP</label>
                                </div>
                                <label class="mt1">Upload Photo KTP</label>
                                <div class="dropzone-wrapper mt0">
                                  <div class="dropzone-desc">
                                      <div class="col-md-4">
                                        <img src="{{asset('dist/images/upload.svg')}}" alt="" srcset="" width="50px">
                                      </div>
                                      <div class="col-md-6">
                                        <h5 style="font-weight: 700;">Drop your file here, or browse</h5>
                                        <small class="info f-grey">Support : .png, .jpg, .jpeg</small>
                                      </div>
                                  </div>
                                  <input type="file" name="img_logo" class="dropzone">
                                </div>
                              </div>
                            </div>
                            <hr>
                            <div class="title-default mb2">Account Statement</div>
                            <div class="table-responsive">
                              <table id="myTable" class="display table middle" style="width: 100%;">
                                <thead>
                                  <tr>
                                    <th>Bank</th>
                                    <th>No Rekening</th>
                                    <th>Nama Pemilik Rekening</th>
                                    <th>Foto Buku Tabungan</th>
                                    <th width="100">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <div class="d-flex align-center">Bank BCA <span class="p-logo p-klikbca"></span></div>
                                    </td>
                                    <td>
                                      <div class="fw-bold">09283847819</div>
                                    </td>
                                    <td>
                                      <div class="fw-bold">Ferry Ardhana</div>
                                    </td>
                                    <td>
                                      <div class="">
                                        <a href="./assets/images/sample-buku-bca.png" alt="" target="_blank"><i class="fa fa-eye"></i> &nbsp;Lihat gambar</a>
                                      </div>
                                    </td>
                                    <td class="flex-center pos-relative">
                                      <button type="button" class="btn btn-default-2 btn-addon" onclick="window.location.href='data-client.html'">Edit</button>
                                      <button type="button" class="btn btn-default btn-addon" onclick="window.location.href='data-client.html'">Delete</button>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <button type="button" class="btn btn-default-2 btn-addon mt1" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus"></i>Add Account Statement</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->
        </div><!-- Main Wrapper -->
    </div><!-- /Page Inner -->
@endsection