<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'nik' => '1234567890123456',
            'fullname' => 'Surya Fauzan',
            'email' => 'suryafauzan@gmail.com',
            'password' => bcrypt('password'),
            'retype_password' => bcrypt('password')
        ]);

        User::factory(10)->create();
        
    }
}
